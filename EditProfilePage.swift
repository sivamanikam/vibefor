//
//  EditProfilePage.swift
//  darcode
//
//  Created by darcode team on 22/03/20.
//  Copyright © 2020 Darcode. All rights reserved.
//

import SwiftUI

struct EditProfilePage: View {
    
    //@State var profileData:UserViewClass
    var api:UserAccountApi = UserAccountApi()
    @Binding var showProfile:Bool
    @State var firstname:String
    @State var lastname:String
    @State var useremail:String
    var user_id = Database().read(table : "UserDetails")[0].employeeId
    
    
    
    var body: some View {
        ZStack{
            LinearGradient(gradient: Gradient(colors: [Color("SignupBottom"),  .black]), startPoint: .top, endPoint: .bottom)
            //Color.blue
            VStack(alignment: .leading){
                //print("in view \(firstname)")
                Button(action: {
                    self.showProfile = false;
                }) {
                    HStack{
                        Spacer()
                        Image(systemName: "xmark").resizable().foregroundColor(Color("white")).frame(width:15,height:15)
                    }
                }
                Text("FirstName").font(.headline).foregroundColor(Color("white"))
                ZStack(alignment: .leading) {
                    if self.firstname.isEmpty { Text("Enter Firstname").foregroundColor(Color("white")).padding() }
                   
                    TextField("", text: $firstname)
                  .foregroundColor(Color("white")).padding()
                     
                  .overlay(
                    RoundedRectangle(cornerRadius:10)
                       
                        .stroke(Color("white"), lineWidth: 1)
                     
                  )
                }
                Text("LastName").font(.headline).foregroundColor(Color("white")).padding(.top, 10)
                ZStack(alignment: .leading) {
                    if self.lastname.isEmpty { Text("Enter Lastname").foregroundColor(Color("white")).padding() }
                   
                    TextField("", text: $lastname)
                  .foregroundColor(Color("white")).padding()
                     
                  .overlay(
                    RoundedRectangle(cornerRadius:10)
                       
                        .stroke(Color("white"), lineWidth: 1)
                     
                  )
                }
                Text("Email").font(.headline).foregroundColor(Color("white")).padding(.top, 10)
                ZStack(alignment: .leading) {
                    if self.useremail.isEmpty { Text("Enter Email").foregroundColor(Color("white")).padding() }
                   
                    TextField("", text: $useremail).disabled(true)
                        .foregroundColor(Color("white")).padding()
                        
                  .overlay(
                    RoundedRectangle(cornerRadius:10)
                       
                        .stroke(Color("white"), lineWidth: 1)
                     
                  )
                }.opacity(0.4)
                Spacer()
                Button(action: {
                   
                    self.showProfile = false;
                    
                    self.api.fetchdata(urlString: "https://api.vibefor.com/users/updateuserdetails", params: ["user_id": String(self.user_id), "flag":"1", "first_name":self.firstname, "last_name":self.lastname]) { (result:String) in
                        print(result)
                    }
                   
                }) {
                  Text("Update").fontWeight(.semibold)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding()
                    .foregroundColor(Color("PrimaryColor"))
                    .background(Color("vibe"))
                    .cornerRadius(10)
                }
                

            }.padding(30)
        }
        
    }
}


