//
//  SettingPage.swift
//  darcode
//
//  Created by darcode team on 18/03/20.
//  Copyright © 2020 Darcode. All rights reserved.
//

import SwiftUI

struct SettingPage: View {
    private var apiFetch = UserViewAPI()
    @State private var userfirstname:String?;
    @State private var userData:UserViewClass?
    @State private var showPage = false;
    @State private var currentTapId = 0;
    @State private var customGreyScreen = false;
    @State private var showActionSheet = false;
    @State private var showConfirm = false;
    @State private var showProfile = false;
    
    @State private var showImagePicker : Bool = false
    @State private var image : Image? = nil
    
    @State private var buttonList = [SettingModel(list_id: 1, list_title: "My Profile",navigate:true),
                                     SettingModel(list_id: 3, list_title: "Manage Topics",navigate:true),
                                     SettingModel(list_id: 4, list_title: "Help",navigate:true),
                                     SettingModel(list_id: 5, list_title: "About",navigate:true),
                                     SettingModel(list_id: 6, list_title: "Logout",navigate:false)]
    
    init() {
        //"My Profile", "Tastemaker", "Reset Password", "Contact", "About", "Logout"
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithTransparentBackground()
        appearance.largeTitleTextAttributes = [
            .font : UIFont.systemFont(ofSize: 20),
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        // this only applies to small titles
        appearance.titleTextAttributes = [
            .font : UIFont.systemFont(ofSize: 20),
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().tintColor = .white
        
        
        self.showPage = false;
        //print("setting called")
        
    }
    var body: some View {
        ZStack{
        ZStack{
            Color.black.edgesIgnoringSafeArea([.all])
            //Color.blue
            VStack(alignment: .leading){
                
                ScrollView{
                    
                    ZStack{
                        VStack{
                            ZStack{
                                if(image != nil)
                                {
                                    image?
                                        .resizable()
                                        .frame(width: 100, height: 100)
                                        .foregroundColor(Color("borderColor"))
                                        .clipShape(Circle())
                                        .shadow(radius: 10)
                                }
                                else
                                {
                                    //EmptyView()
                                    getimgView()
                                }
                                Image(systemName: "camera.fill")
                                .resizable()
                                .foregroundColor(Color("vibe"))
                                .frame(width: 25, height: 25)
                                .offset(x: 35, y: 35)
                            }
                            
                            Text(userfirstname ?? "Username").font(.headline)
                        .lineLimit(1).foregroundColor(Color("white"))
                            .padding();
                            
                        //getNameView()
                    }

                }.frame(height:220).onTapGesture {
                    self.showImagePicker.toggle()
                }.sheet(isPresented : self.$showImagePicker){
                    PhotoCaptureView(showImagePicker: self.$showImagePicker, image: self.$image)
                }
                    .onAppear(){
                        self.apiFetch.fetchdata { (resp) in
                            if resp != nil {
                                print("setting called")
                                self.userData = resp;
                                self.userfirstname = "\(self.userData?.first_name ?? "") \(self.userData?.last_name ?? "")"
                            }
                        }
                    }
                    
                    
                VStack(alignment: .leading){

                    ForEach(0 ..< buttonList.count, id:\.self) { row in

                        Button(action: {
                            
                            self.currentTapId = self.buttonList[row].list_id;
                            if(self.buttonList[row].navigate)
                            {
                                if(self.currentTapId == 1)
                                {
                                    self.showProfile.toggle()
                                }
                                else
                                {
                                    self.showPage = true;
                                    self.showActionSheet = false;
                                }
                            }
                            else
                            {
                                self.customGreyScreen = true;
                                self.showPage = false;
                                self.showActionSheet=true;
                                print(self.currentTapId)
                            }
                        })
                            {
                        ZStack{
                            RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color("PrimaryColor")).shadow(radius: 2)
                            HStack{
                                Text(self.buttonList[row].list_title).font(.headline)
                                    .lineLimit(1).foregroundColor(Color("white"));
                                Spacer()
                                if self.buttonList[row].navigate  {
                                    Image(systemName:"chevron.right").resizable().foregroundColor(Color.white).frame(width:10,height:10) }
                                else {
                                    EmptyView()
                                }
                            }.padding()
                        }.frame(height: 75).padding(EdgeInsets(top: 0, leading: 15, bottom: 5, trailing: 15))
                        }.buttonStyle(PlainButtonStyle())
                        .actionSheet(isPresented: self.$showActionSheet) {
                            ActionSheet(
                                title: Text("Logout"),
                                message: Text("Are you sure you want to Logout?"),
                                buttons: [
                                    ActionSheet.Button.destructive(Text("Logout")) {
                                        self.customGreyScreen.toggle()
//                                        if(self.currentTapId == 3)
//                                        {
//                                            self.showConfirm.toggle()
//                                        }
                                        
                                    },
                                    ActionSheet.Button.cancel(){
                                        self.customGreyScreen.toggle()
                                    }
                                ]
                            )
                        }
                        .alert(isPresented: self.$showConfirm) {
                            Alert(title: Text("Alert"), message: Text("We’ve sent you an email with a link to reset your password"), dismissButton: .default(Text("Ok")))
                        }
                        .sheet(isPresented: self.$showProfile)
                        {
                            
                            EditProfilePage(showProfile: self.$showProfile, firstname: self.userData?.first_name ?? "", lastname: self.userData?.last_name ?? "", useremail: self.userData?.email ?? "")
                        }
                    }


                }

                }

            }.padding(EdgeInsets(top: 20, leading: 0, bottom: 20, trailing: 0))
            
            
            ZStack(){
                Color.black
                }.opacity(self.customGreyScreen ? 0.7 : 0).onTapGesture {
                self.customGreyScreen = false;
                    self.showPage = false;
            }
            
            
            
            if (showPage)
            {
                if(currentTapId == 3)//contact
                {
                    NavigationLink(destination: TagView(), isActive: self.$showPage) {
                        EmptyView()
                    }
                    
                }
                else if(currentTapId == 4)//contact
                {
                    NavigationLink(destination: ContactPage(), isActive: self.$showPage) {
                        EmptyView()
                    }
                    
                }
                else if(currentTapId == 5)//about
                {
                    NavigationLink(destination: AboutPage(), isActive: self.$showPage) {
                        EmptyView()
                    }
                    
                    
                }
            }
            
            
            }.navigationBarTitle("Setting", displayMode: .inline)
        }
        
        
    }
    
    func reloadData() -> Void {
        DispatchQueue.main.asyncAfter(deadline:.now() + 2.0)
        {
        print("reload data called")
        self.apiFetch.fetchdata { (resp) in
            if resp != nil {
                                           print("setting called")
                                           self.userData = resp;
                                           self.userfirstname = "\(self.userData?.first_name ?? "") \(self.userData?.last_name ?? "")"
                                       }
        }
        }
    }
    
    func getimgView() -> AnyView {
        if let imagename = self.userData?.user_image
        {
            return AnyView(ImageView(withURL: imagename)
            .frame(width: 150, height: 150)
            .foregroundColor(Color("white"))
            .clipShape(Circle())
            .shadow(radius: 10)
            )
        } else {
            return AnyView(Image(systemName: "person.crop.circle.fill")
            .resizable()
            .frame(width: 100, height: 100)
                .foregroundColor(Color.gray)
            .clipShape(Circle())
            .shadow(radius: 10))
        }
    }
    
    func getNameView() -> AnyView {
        return AnyView(EmptyView())
    }
}

struct SettingPage_Previews: PreviewProvider {
    static var previews: some View {
        SettingPage()
    }
}

struct SettingModel: Codable, Identifiable {
    public var id = UUID()
    public var list_id: Int
    public var list_title: String
    public var navigate: Bool
    
    
}


