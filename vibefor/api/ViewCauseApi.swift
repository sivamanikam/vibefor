//
//  UserViewAPI.swift
//  app
//
//  Created by Sunil on 12/02/20.
//  Copyright © 2020 darcode team. All rights reserved.
//

import SwiftUI
import Foundation
import Combine

public class ViewCauseApi: ObservableObject {
    
    
    @Published var userlist:[VibeUserModel] = [VibeUserModel]()
    
    
    init(){
        
    }
    
    
    
    func fetchdata<T:Decodable>(urlString:String, finish: @escaping (T) -> ()) {

        guard let url = URL(string: urlString) else {
            return
        }
        let request = URLRequest(url: url)
        
        
        URLSession.shared.dataTask(with: request) {(data,response,error) in
            if error != nil {
                
                return;
            }
            
            if let d = data {
                do
                {
                let decodedLists = try JSONDecoder().decode(T.self, from: d)
                    finish(decodedLists)
                    //self.fetchUserdata(urlString: "https://api.vibefor.com/channels/users/552")
                }
                catch
                {
                    
                }
            }else {
                //finish(nil);
                print("No Data")
                
            }


        }.resume()
    }
        
        func fetchUserdata(urlString:String) {
        print("fetch userdata \(urlString)" )
        guard let url = URL(string: urlString) else {
            print("error")
            return
        }
        let request = URLRequest(url: url)
        print("section 1")
        
        URLSession.shared.dataTask(with: request) {(data,response,error) in
            if error != nil {
                print("error")
                return;
            }
            
            if let d = data {
                do
                {
                    
                let decodedLists = try JSONDecoder().decode([VibeUserModel].self, from: d)
                    print(decodedLists)
                    DispatchQueue.main.async {
                        print(decodedLists)
                        self.userlist = decodedLists
                        
                    }
                }
                catch
                {
                    print("error parsing \(error)")
                }
            }else {
                //finish(nil);
                print("No Data")
                
            }


        }.resume()

    }
    
}

struct VibeUserModel:Decodable {
    
    var user_id:Int
    var user_image:String?
    var user_name: String
    var vibe_duration:Int
    
}

struct CauseDetailModal :Decodable {
    
    
    let id = UUID()
    var channel_id: Int
    var channel_name: String
    var channel_desc: String?
    var channel_image :String?
    let channel_type :Int
    let user_id :Int
    let user_name :String?
    let user_image:String?
    let tags :String?
    
    
    
    
}





