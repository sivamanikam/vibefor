//
//  UserViewAPI.swift
//  app
//
//  Created by Sunil on 12/02/20.
//  Copyright © 2020 darcode team. All rights reserved.
//

import SwiftUI
import Foundation
import Combine

public class UserAccountApi: ObservableObject {
    
    //var userData:UserViewClass?
    //var userFirstname:String?
    typealias Parameters = [String : String]
    
    init(){
        
    }
    
    
    
    func fetchdata<T>(urlString:String, params:[String: String],  finish: @escaping (T) -> ()) {

        
        let parameters = params

        guard let url = URL(string: urlString) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let boundary = generateBoundary()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let dataBody = createDataBody(withParameters: parameters, boundary: boundary)
        request.httpBody = dataBody
        
        
        URLSession.shared.dataTask(with: request) {(data,response,error) in
            if error != nil {
                finish("Fail" as! T);
                return;
            }
            DispatchQueue.main.async {
                //self.userData = decodedLists[0]
                finish("Success" as! T);
            }


        }.resume()

    }
    
    func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createDataBody(withParameters params: Parameters?, boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        if let parameters = params {
            for (key, value) in parameters {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value + lineBreak)")
            }
        }
        
        
        
        body.append("--\(boundary)--\(lineBreak)")
        
        return body
    }
}






