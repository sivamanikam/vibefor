//
//  UserViewAPI.swift
//  app
//
//  Created by Sunil on 12/02/20.
//  Copyright © 2020 darcode team. All rights reserved.
//

import SwiftUI
import Foundation
import Combine

public class UserViewAPI: ObservableObject {
    
    typealias Parameters = [String: String]
    
    var user_id = Database().read(table : "UserDetails")[0].employeeId
    
    //var userData:UserViewClass?
    //var userFirstname:String?
    
    init(){
        
    }
    
    
    
    func fetchdata( finish: @escaping (_ resp: UserViewClass?) -> ()) {
        print("user id - \(user_id)")
        
        let parameters = ["user_id": String(user_id)]
        
        guard let url = URL(string: "https://api.vibefor.com/users/getuserbyid") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let boundary = generateBoundary()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let dataBody = createDataBody(withParameters: parameters, boundary: boundary)
        request.httpBody = dataBody
        
        URLSession.shared.dataTask(with: request) {(data,response,error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([UserViewClass].self, from: d)
                    DispatchQueue.main.async {
                        //self.userData = decodedLists[0]
                        print(decodedLists[0])
                        finish(decodedLists[0]);
                    }
                }else {
                    finish(nil);
                    print("No Data")
                }
            } catch {
                print ("Error")
                finish(nil);
            }


        }.resume()

    }
    
    func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createDataBody(withParameters params: Parameters?, boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        if let parameters = params {
            for (key, value) in parameters {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value + lineBreak)")
            }
        }
        
        
        
        body.append("--\(boundary)--\(lineBreak)")
        
        return body
    }
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}



struct UserViewAPIModel: Decodable {
    
    public var success: Bool
    public var payload: [UserViewClass]
    
    enum CodingKeys: String, CodingKey {
        
        case success = "success"
        case payload = "payload"
    }
}
struct UserViewClass :Decodable {
    
    
    let id = UUID()
    var user_id: String
    var first_name: String?
    var last_name: String?
    var user_image :String?
    let email :String?
    let service :String?
    let status :Int
    
    
    
    
}
