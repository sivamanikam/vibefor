//
//  SettingPage.swift
//  darcode
//
//  Created by darcode team on 18/03/20.
//  Copyright © 2020 Darcode. All rights reserved.
//

import SwiftUI

struct CauseInfo: View {
     @ObservedObject private var apiFetch = ViewCauseApi()
    
    
    @State private var showImagePicker : Bool = false
    @State private var image : Image? = nil
    @State private var currentCause:CauseDetailModal?
    @State var causeDesc:String?
    
    @State private var buttonList = [CauseModel(list_id: 1, list_title: "Cause",navigate:false),
                                     CauseModel(list_id: 2, list_title: "Created by",navigate:false),
                                     CauseModel(list_id: 3, list_title: "About the cause",navigate:false),
                                     CauseModel(list_id: 4, list_title: "Enable Donations",navigate:false),
                                     CauseModel(list_id: 5, list_title: "Tags",navigate:false)]
    
    
    
    init() {
        //"My Profile", "Tastemaker", "Reset Password", "Contact", "About", "Logout"
        
        
    }
    var body: some View {
        ZStack{
        ZStack {
            Color.black.edgesIgnoringSafeArea([.all])
            //Color.blue
            VStack(alignment: .leading)
            {
                
                ScrollView{
                    
                    ZStack{
                        if(image != nil)
                        {
                            image?
                                .resizable()
                                .frame(height: 200)
                                .foregroundColor(Color("borderColor"))
                                .clipShape(RoundedRectangle(cornerRadius: 5))
                                .shadow(radius: 10)
                        }
                        else
                        {
                            //EmptyView()
                            getimgView()
                        }

                }.frame(height:220).onTapGesture {
                    self.showImagePicker.toggle()
                }.sheet(isPresented : self.$showImagePicker){
                    PhotoCaptureView(showImagePicker: self.$showImagePicker, image: self.$image)
                }
                    .onAppear{
                        self.apiFetch.fetchdata(urlString: "https://api.vibefor.com/channels/details/552")
                        { (resp:CauseDetailModal) in
                            self.currentCause = resp;
                            self.causeDesc = self.currentCause?.channel_desc ?? ""
                        };
                        self.apiFetch.fetchUserdata(urlString: "https://api.vibefor.com/channels/users/552")
                    }
                    
                    
                VStack(alignment: .leading){

                    ForEach(0 ..< buttonList.count, id:\.self) { row in
                        
                        Button(action: {
                            
                           
                        })
                            {
                        self.getCauseView(id: self.buttonList[row].list_id);
                                //self.getDonationView(id: self.buttonList[row].list_id);
                        }.buttonStyle(PlainButtonStyle())
                        
                    }

                    getParticipantView();
                    }

                }.onAppear(){
                    
                }

            }.padding(EdgeInsets(top: 20, leading: 0, bottom: 20, trailing: 0))
                            
            
            
            
            }.navigationBarTitle("CauseInfo", displayMode: .inline)
        }
        
        
    }
    
    func getDonationView(id:Int) -> AnyView {
    if id == 4{
    return AnyView(
        ZStack{
        RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color("PrimaryColor")).shadow(radius: 2)
            
            HStack(){
                Text(self.buttonList[id-1].list_title).font(.headline)
            .lineLimit(1).foregroundColor(Color("vibe"));
               Spacer()
            }.padding()
            
        }.fixedSize(horizontal: false, vertical: true).padding(EdgeInsets(top: 0, leading: 15, bottom: 5, trailing: 15))
    );
    }
    else
    {
        return AnyView(EmptyView())
    }
    }
    
    func getParticipantView() -> AnyView {
        AnyView(ZStack{
            RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color("PrimaryColor")).shadow(radius: 2)
                HStack(alignment: .top){
                VStack(alignment: .leading, spacing: 10){
                Text("Participants").font(.headline)
                .lineLimit(1).foregroundColor(Color("vibe"));
                    ForEach(0 ..< self.apiFetch.userlist.count, id:\.self) { row in
                        HStack{
                            self.getUserimgView(url: self.apiFetch.userlist[row].user_image)
                    Text(self.apiFetch.userlist[row].user_name).font(.subheadline)
                    .lineLimit(1).foregroundColor(Color("white"));
                            Spacer()
                            Text("\(self.apiFetch.userlist[row].vibe_duration) sec").font(.subheadline)
                            .lineLimit(1).foregroundColor(Color("white"));
                        }
                }
                    
                }
                    Spacer()
                }.padding()
        }.fixedSize(horizontal: false, vertical: true).padding(EdgeInsets(top: 0, leading: 15, bottom: 5, trailing: 15)));
    }
    
    func getUserimgView(url:String?) -> AnyView {
        
        if let imagename = url
        {
            return AnyView(ImageView(withURL: "https://dev-vibe.s3.amazonaws.com/\(imagename)")
            .frame(width: 30, height: 30)
            .foregroundColor(Color("white"))
            .clipShape(Circle())
            .shadow(radius: 10)
            )
        } else {
            return AnyView(Image(systemName: "person.crop.circle.fill")
            .resizable()
            .frame(width: 30, height: 30)
                .foregroundColor(Color.gray)
            .clipShape(Circle())
            .shadow(radius: 10))
        }
    }
    
    func getimgView() -> AnyView {
        if let imagename = self.currentCause?.channel_image
        {
            return AnyView(ImageView(withURL: "https://dev-vibe.s3.ap-south-1.amazonaws.com/\(imagename)")
            .frame(height: 200)
            .foregroundColor(Color("white"))
            .clipShape(RoundedRectangle(cornerRadius: 5))
            .shadow(radius: 10)
                .padding()
            )
        } else {
            return AnyView(Image("")
            .resizable()
            .frame(height: 200)
            .background(Color("PrimaryColor"))
            .clipShape(RoundedRectangle(cornerRadius: 5))
            .shadow(radius: 10)
            .padding())
        }
    }
    
    
    func getCauseView(id:Int) -> AnyView
    {
        if id == 1
        {
            let data:String = self.currentCause?.channel_name ?? ""
            
            
            
            return AnyView(
                ZStack{
                RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color("PrimaryColor")).shadow(radius: 2)
                HStack(alignment: .top){
                VStack(alignment: .leading, spacing: 5){
                Text(self.buttonList[id-1].list_title).font(.headline)
                .lineLimit(1).foregroundColor(Color("vibe"));
                    
                    Text(data).font(.headline)
                .lineLimit(1).foregroundColor(Color("white"));
                }
                    Spacer()
                    if self.buttonList[id-1].navigate  {
                        Image(systemName:"chevron.right").resizable().foregroundColor(Color.white).frame(width:10,height:10) }
                    else {
                        EmptyView()
                    }
            }.padding()
            }.fixedSize(horizontal: false, vertical: true).padding(EdgeInsets(top: 0, leading: 15, bottom: 5, trailing: 15)))
        }
        else if id == 2
        {
            let data:String = self.currentCause?.user_name ?? ""
            let userimage:String = self.currentCause?.user_image ?? ""
            return AnyView(
                ZStack{
                RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color("PrimaryColor")).shadow(radius: 2)
                HStack(alignment: .top){
                VStack(alignment: .leading, spacing: 5){
                Text(self.buttonList[id-1].list_title).font(.headline)
                .lineLimit(1).foregroundColor(Color("vibe"));
                    HStack{
                        getUserimgView(url:userimage)
                    Text(data).font(.headline)
                .lineLimit(1).foregroundColor(Color("white"));
                    }
                }
                    Spacer()
                            if self.buttonList[id-1].navigate  {
                                Image(systemName:"chevron.right").resizable().foregroundColor(Color.white).frame(width:10,height:10) }
                            else {
                                EmptyView()
                            }
                    }.padding()
                    }.fixedSize(horizontal: false, vertical: true).padding(EdgeInsets(top: 0, leading: 15, bottom: 5, trailing: 15))
            )
        }
            else if id == 3
            {
                //let data:String = self.currentCause?.channel_desc ?? ""
                return AnyView(
                    ZStack{
                    RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color("PrimaryColor")).shadow(radius: 2)
                    HStack(alignment: .top){
                    VStack(alignment: .leading, spacing: 5){
                    Text(self.buttonList[id-1].list_title).font(.headline)
                    .lineLimit(1).foregroundColor(Color("vibe"));
                        
                        Text(self.currentCause?.channel_desc ?? "").font(.headline).multilineTextAlignment(.leading)
                            .lineLimit(nil).fixedSize(horizontal: false, vertical: true).foregroundColor(Color("white"));
                        
                        
                        
                    }.fixedSize(horizontal: false, vertical: true)
                        Spacer()
                                if self.buttonList[id-1].navigate  {
                                    Image(systemName:"chevron.right").resizable().foregroundColor(Color.white).frame(width:10,height:10) }
                                else {
                                    EmptyView()
                                }
                        }.padding()
                        }.fixedSize(horizontal: false, vertical: true).padding(EdgeInsets(top: 0, leading: 15, bottom: 5, trailing: 15))
                )
            }
            else if id == 4
            {
                return AnyView(EmptyView())
            }
            else if id == 5
            {
                var data:String = self.currentCause?.tags ?? ""
                var tagArray:[String] = [String]()
                if(data != "")
                {
                    data = String(data.dropFirst());
                    data = String(data.dropLast());
                    data = data.replacingOccurrences(of: "'", with: "");
                    tagArray = data.components(separatedBy: ",");
                }
                
                //print(changearray);
                return AnyView(
                    ZStack{
                    RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color("PrimaryColor")).shadow(radius: 2)
                    HStack(alignment: .top){
                    VStack(alignment: .leading, spacing: 5){
                    Text(self.buttonList[id-1].list_title).font(.headline)
                    .lineLimit(1).foregroundColor(Color("vibe"));
                        
                        Text(data).font(.headline).multilineTextAlignment(.leading)
                            .lineLimit(nil).fixedSize(horizontal: false, vertical: true).foregroundColor(Color("white"));
                    }.fixedSize(horizontal: false, vertical: true)
                Spacer()
                                if self.buttonList[id-1].navigate  {
                                    Image(systemName:"chevron.right").resizable().foregroundColor(Color.white).frame(width:10,height:10) }
                                else {
                                    EmptyView()
                                }
                        }.padding()
                        }.fixedSize(horizontal: false, vertical: true).padding(EdgeInsets(top: 0, leading: 15, bottom: 5, trailing: 15))
                )
            }
        else
        {
            return AnyView(EmptyView())
        }
    }
    
    func getNameView() -> AnyView {
        return AnyView(EmptyView())
    }
}

struct CauseInfo_Previews: PreviewProvider {
    static var previews: some View {
        CauseInfo()
    }
}

struct CauseModel: Codable, Identifiable {
    public var id = UUID()
    public var list_id: Int
    public var list_title: String
    public var navigate: Bool
    
    
}




