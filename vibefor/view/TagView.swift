//
//  TagView.swift
//  vibeapp
//
//  Created by darcode team on 27/04/20.
//  Copyright © 2020 darcode team. All rights reserved.
//

import SwiftUI
import WaterfallGrid

struct TagView: View {
//
//  @State private var buttonList =
//    [
//      AboutModel(list_id: 1, list_title: "Animals",navigate:false),
//      AboutModel(list_id: 2, list_title: "Art",navigate:false),
//      AboutModel(list_id: 1, list_title: "Children's Right",navigate:false),
//      AboutModel(list_id: 2, list_title: "Civil Rights",navigate:false),
//      AboutModel(list_id: 1, list_title: "COVID-19",navigate:false),
//      AboutModel(list_id: 2, list_title: "Education",navigate:false),
//      AboutModel(list_id: 1, list_title: "Enviorment",navigate:false),
//      AboutModel(list_id: 2, list_title: "Health",navigate:false),
//      AboutModel(list_id: 1, list_title: "Hunger",navigate:false),
//      AboutModel(list_id: 2, list_title: "Safety",navigate:false),
//      AboutModel(list_id: 2, list_title: "LGBTQ",navigate:false),
//      AboutModel(list_id: 2, list_title: "Human Rights",navigate:false),
//      AboutModel(list_id: 2, list_title: "Meditation",navigate:false),
//      AboutModel(list_id: 3, list_title: "Poverty",navigate:false),
//      AboutModel(list_id: 0, list_title: "",navigate:false),
//      AboutModel(list_id: 0, list_title: "",navigate:false)
//  ]
   @State private var buttonList = [
     TagModel( id: 26, label: "Animals",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/animal.jpg",selected:false),
     TagModel( id: 33, label: "Art",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/art.jpg",selected:false),
     TagModel( id: 94, label: "Children's Right",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Childrens-Rights.jpg" ,selected:false),
    TagModel( id: 101, label: "Civil Rights",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Civil-Rights.jpg" ,selected:false),
     TagModel( id: 1001, label: "COVID-19" , src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/COVID-19.jpg" ,selected:false),
     TagModel( id: 204, label: "Education",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Education.jpg" ,selected:false),
     TagModel( id: 222, label: "Environment",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Environment.jpg" ,selected:false),
     TagModel( id: 314, label: "Health",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Health.jpg" ,selected:false),
     TagModel( id: 339, label: "Human Rights",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Human-Rights.jpg" ,selected:false),
     TagModel( id: 343, label: "Hunger",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Hunger.jpg" ,selected:false),
     TagModel( id: 399, label: "LGBTQ",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/LGBTQ.jpg" ,selected:false),
     TagModel( id: 427, label: "Meditation",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/meditation.jpg" ,selected:false),
     TagModel( id: 521, label: "Poverty",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Poverty.jpg" ,selected:false),
     TagModel( id: 576, label: "Safety",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Safety.jpg" ,selected:false),
     TagModel( id: 629, label: "Sports",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Sports.jpg" ,selected:false),
     TagModel( id: 744, label: "Women Right's",src:"https://dev-vibe.s3.amazonaws.com/vibe/topic_images/Womens-Rights.jpg",selected:false)
    ];
    
  @State var tagArray : [String] = []
  @State private var mainView = false
  init()
  {
//    let appearance = UINavigationBarAppearance()
//    // this overrides everything you have set up earlier.
//    appearance.configureWithTransparentBackground()
//    // this only applies to big titles
//    appearance.largeTitleTextAttributes = [
//      .font : UIFont.systemFont(ofSize: 20),
//      NSAttributedString.Key.foregroundColor : UIColor.white
//    ]
//    // this only applies to small titles
//    appearance.titleTextAttributes = [
//      .font : UIFont.systemFont(ofSize: 20),
//      NSAttributedString.Key.foregroundColor : UIColor.white
//    ]
//    //In the following two lines you make sure that you apply the style for good
//    UINavigationBar.appearance().scrollEdgeAppearance = appearance
//    UINavigationBar.appearance().standardAppearance = appearance
//    // This property is not present on the UINavigationBarAppearance
//    // object for some reason and you have to leave it til the end
//    UINavigationBar.appearance().tintColor = .white
  }
    var body: some View {
      ZStack {
        LinearGradient(gradient: Gradient(colors: [Color("PrimaryColor"), .black]), startPoint: .top, endPoint: .bottom).edgesIgnoringSafeArea([.all])
          //.edgesIgnoringSafeArea([.all])
        VStack(alignment : .leading, spacing: 10) {
            Text("What are you interested in?").font(.title)
            //                .padding(.horizontal,10).padding(.vertical,10)
            .animation(Animation.interpolatingSpring(stiffness: 40, damping: 7).delay(0.1))
          Text("Pick your topics and we'll help you to find the cause that matters to you.")
            .padding(.horizontal,10).font(.subheadline)
            
            
            
          WaterfallGrid((0..<buttonList.count), id: \.self) { index in
            ZStack {
               ImageView(withURL: self.buttonList[index].src).frame(minWidth : 0,maxWidth : .infinity,minHeight: 0,maxHeight: 100).opacity(0.4)
                VStack{
                              HStack{
                                Spacer()
                                Image(systemName:  self.buttonList[index].selected ? "checkmark.circle.fill" : "checkmark.circle")
                                  .foregroundColor(Color(self.buttonList[index].selected ? "vibe" : "white"))
                                  .font(.headline)
                              }
                              Spacer()
                    Text("\(self.buttonList[index].label)").foregroundColor(Color(self.buttonList[index].selected ? "vibe" : "white")).font(.headline)
                              Spacer()
                            }.onTapGesture {
                              self.buttonList[index].selected.toggle()
                              self.tagArray.append(String(self.buttonList[index].id) + "#" + self.buttonList[index].label)
                }.padding(.all,10)
            }.background(Color.black)
            
            .cornerRadius(6)
            .frame(minWidth : 0,maxWidth : .infinity,minHeight: 0,maxHeight: 100)

          }.padding(.top,10)
          
        }.padding(20)
      }.foregroundColor(.white)
        
      //      .navigationBarHidden(true)
    }
  }


