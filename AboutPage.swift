//
//  AboutPage.swift
//  darcode
//
//  Created by darcode team on 18/03/20.
//  Copyright © 2020 Darcode. All rights reserved.
//

import SwiftUI

struct AboutPage: View {
    
   @State private var showPage:Bool = false;
    
    @State private var buttonList = [AboutModel(list_id: 1, list_title: "About Us",navigate:true),
                                     AboutModel(list_id: 2, list_title: "Terms & Conditions",navigate:true),
                                     AboutModel(list_id: 3, list_title: "Privacy Policy",navigate:true)]
    
    init() {
        //"My Profile", "Tastemaker", "Reset Password", "Contact", "About", "Logout"
    }
    var body: some View {
        ZStack{
            Color.black.edgesIgnoringSafeArea([.all])
            //Color.red
            VStack(alignment: .leading){
//                HStack{
//                    Image(systemName:"chevron.left").renderingMode(.original).resizable().frame(width:10,height:10)
//                    Spacer()
//                    Text("About").font(.headline)
//                    .lineLimit(1).foregroundColor(Color("PrimaryColor"))
//                    Spacer()
//                    }.padding().onTapGesture {
//                     self.showPage = true;
//                }
                ScrollView{
                
                VStack(alignment: .leading){
                    
                    ForEach(0 ..< buttonList.count, id:\.self) { row in
                        
                        Button(action: {
                            if(self.buttonList[row].navigate)
                            {
                                print("Click enabled")
                                if(self.buttonList[row].list_id == 1)
                                {
                                    if let url = URL(string: "https://www.vibefor.com/about-us") {
                                        UIApplication.shared.open(url)
                                    }
                                    //self.showPage.toggle()
                                }
                                else if(self.buttonList[row].list_id == 2)
                                {
                                    if let url = URL(string: "https://vibefor.com/term.html") {
                                        UIApplication.shared.open(url)
                                    }
                                    //self.showPage.toggle()
                                }
                                else if(self.buttonList[row].list_id == 3)
                                {
                                    if let url = URL(string: "https://vibefor.com/policy.html") {
                                        UIApplication.shared.open(url)
                                    }
                                    //self.showPage.toggle()
                                }
                            }
                        })
                            {
                                LinkBtnView(label: self.buttonList[row].list_title, navigate: self.buttonList[row].navigate)
                        }.buttonStyle(PlainButtonStyle())
                    }
                    
                    
                }.padding(EdgeInsets(top: 20, leading: 0, bottom: 30, trailing: 0))
                
            }
            }
            
        }
    }
}

struct AboutPage_Previews: PreviewProvider {
    static var previews: some View {
        AboutPage()
    }
}

struct AboutModel: Codable, Identifiable {
    public var id = UUID()
    public var list_id: Int
    public var list_title: String
    public var navigate: Bool
    
    
}

